#!/usr/bin/env bash

# Ask for the administrator password upfront
sudo -v

# Keep-alive: update existing `sudo` time stamp until `.osx` has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
set -o verbose

## these will uninstall previous versions

# # sudo rm -rf $HOME/.nvm
# PREFIX="${PREFIX:-/usr/local}"
# # erase all possible install paths
# sudo rm -rf $HOME/{local,lib,include,node*,npm,.npm*}
# sudo rm -rf $PREFIX/lib/node*
# sudo rm -rf $PREFIX/include/node*
# sudo rm -rf $PREFIX/bin/{node,npm}
# sudo rm -rf $PREFIX/share/man/man1/node.1
# sudo rm -rf $PREFIX/lib/dtrace/node.d

# curl "https://nodejs.org/dist/latest/node-${VERSION:-$(wget -qO- https://nodejs.org/dist/latest/ | sed -nE 's|.*>node-(.*)\.pkg</a>.*|\1|p')}.pkg" > "$HOME/Downloads/node-latest.pkg" && sudo installer -store -pkg "$HOME/Downloads/node-latest.pkg" -target "/"

# brew install nvm
# mkdir ~/.nvm
# export NVM_DIR=~/.nvm
# source $(brew --prefix nvm)/nvm.sh
# nvm install node
# nvm alias default node



# rm -rf /usr/local/lib/node_modules
# brew install node --without-npm
# echo prefix=~/.node >> ~/.npmrc
# curl -L https://www.npmjs.com/install.sh | sh

source ./npm.sh
