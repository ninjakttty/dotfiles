# Path to your oh-my-zsh installation.
export ZSH=/Users/yuri/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="yuri"


#pull in private infomation
source ~/.private

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="mm-dd-yyyy"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=~/projects/dotfiles/oh-my-zsh/custom

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(bower brew brew-cask colored-man colorize copydir copyfile  cp emoji ember-cli  git git-extras git-flow git-prompt gitignore grunt gulp history-substring-search jsontools man nmap node npm nvm osx pbcopy rsync  sublime sudo textmate urltools zsh-completions)

# User configuration

export PATH="/usr/local/sbin:/usr/local/bin:/Users/yparsons/bin:/Users/yuri/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/MacGPG2/bin:/usr/local/opt/coreutils/libexec/gnubin"

export MANPATH="/usr/local/man:$MANPATH"

# export MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
# set coreutils as default
# PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"



source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='mvim'
fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.




zstyle ':completion:*' verbose yes
zstyle ':completion:*:paths' accept-exact '*(N)' # performance?

zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache


# fuzzy matching for error correcting
zstyle ':completion:*' completer _complete _match _approximate
zstyle ':completion:*:match:*' original only
zstyle ':completion:*:approximate:*' max-errors 1 numeric

#Ignore completion functions for commands you don’t have:
zstyle ':completion:*:functions' ignored-patterns '_*'

#Completing process IDs with menu selection:
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*'   force-list always

#If you end up using a directory as argument, this will remove the trailing slash (usefull in ln)
zstyle ':completion:*' squeeze-slashes true

#cd will never select the parent directory (e.g.: cd ../<TAB>):
zstyle ':completion:*:cd:*' ignore-parents parent pwd


#lets npm modules be found
zstyle ':completion:*' rehash true

#History
export HIST_IGNORE_SPACE="true"
export HIST_IGNORE_DUPS="true"
export SHARE_HISTORY="true"
export EXTENDED_HISTORY="true"

source ~/.alias

# Node Stuff
export NVM_DIR=~/.nvm
export NODE_ENV=develop
source $(brew --prefix nvm)/nvm.sh

# nvm use --silent  stable


setopt autocd                   # change to dirs without cd
setopt pushd_to_home            # Push to home directory when no argument is given.
setopt auto_pushd               # Push the old directory onto the stack on cd.
setopt auto_name_dirs           # Auto add variable-stored paths to ~ list.
setopt pushd_ignore_dups        # Do not store duplicates in the stack.

test -e ${HOME}/.iterm2_shell_integration.zsh && source ${HOME}/.iterm2_shell_integration.zsh
export PATH=$HOME/local/bin:$PATH


autoload -Uz compinit  && compinit


