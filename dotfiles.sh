#!/bin/sh

# Ask for the administrator password upfront
# sudo -v
set -o verbose
# ln -s ~/projects/dotfiles/.alias ~/.alias
# ln -s ~/projects/dotfiles/.private ~/.private

for file in ${HOME}/projects/dotfiles/dotfiles/*; do
	fileName=$(basename "$file")
	case "alias" in
		$fileName )
				`echo "ln -s $file ${HOME}/.$fileName"`
				`echo "ln -s $file ${HOME}/.z$fileName"`;;
		* )
				`echo "ln -s $file ${HOME}/.$fileName"`;;
	esac

	# `echo "ln -s $file ${HOME}/.$(basename "$file")"`
done

