#!/usr/bin/env bash

# Keep-alive: update existing `sudo` time stamp until `.osx` has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# make sure permissions are all generator-code
# sudo chown -R $(whoami) $(npm config get prefix)/{lib/node_modules,bin,share}

sudo chown -R $(whoami):admin $(npm config get prefix)/{lib/node_modules,bin,share}
set -o verbose
npm set progress='false'
#builders
npm install -g browserify
npm install -g handlebars
npm install -g yo
npm install -g jscs
npm install -g htmltidy
npm install -g jshint
npm install -g less #less compiler
npm install -g babel
npm install -g generator-code
npm install -g slush

#task runnners & servers
npm install -g bower
npm install -g broccoli
npm install -g coffee
npm install -g ember-cli
npm install -g grunt-cli
npm install -g gulp-cli
npm install -g node-inspector
npm install -g forever
npm install -g nodemon
npm install -g simple-server
npm install -g weinre

#JSON
npm install -g jsome
npm install -g jq

#testing
npm install -g mocha
npm install -g jasmine
npm install -g tape

#other
npm install -g rimraf
npm install -g nipster
npm install -g trash-cli
npm install -g vsce
npm install -g ntypescript
# npm init if installing
npm install -g npm-autoinit
npm install -g npm-check-updates


# npm config set progress='true'
npm config set onload-script='npm-autoinit/autoinit'
npm config set progress='true'
npm config set save='true'
npm config set python '/usr/local/bin/python2.7'
# npm config set dev='true'
npm config set also='true'
npm config set npat='true'

npm config set init-author-email='yuri@ninjakitty.net'
npm config set init-author-name='Yuri A. Parsons'
npm config set init-author-url='http://ninjakitty.net'
npm config set init-version='0.0.1'
npm config set editor='vim'
