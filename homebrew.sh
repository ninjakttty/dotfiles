#!/usr/bin/env bash

# Ask for the administrator password upfront
# sudo -v

# Keep-alive: update existing `sudo` time stamp until `.osx` has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
set -o verbose
#install homebrew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew install caskroom/cask/brew-cask
brew tap caskroom/fonts
brew tap homebrew/dupes
brew tap caskroom/versions


# super important stuff needed right away
brew cask install iterm2
brew cask install dropbox

# use a good shell
brew install zsh
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
ln -s ~/projects/dotfiles/.zshrc ~/.zshrc
git clone https://github.com/zsh-users/zsh-completions ~/.oh-my-zsh/custom/plugins/zsh-completions
rm -f ~/.zcompdump; compinit

brew cask install sublime-text3
brew cask install alfred
brew cask install moom # from MAS see app_store.sh
brew cask install google-chrome

brew install wget
brew install nmap
brew install git-flow
brew install ssh-copy-id
brew install ack
brew install coreutils
brew install findutils
brew install moreutils
brew install the_silver_searcher

###############################################################################
# Fonts                                                                       #
###############################################################################
brew cask install font-inconsolata-dz
brew cask install font-inconsolata-dz-for-powerline
brew cask install font-source-code-pro
brew cask install font-source-code-pro-for-powerline
brew cask install font-inconsolata-dz
brew cask install font-source-code-pro
brew cask install font-comic-sans-ms
brew cask install font-devicons
brew cask install font-meslo-lg
brew cask install font-meslo-lg-for-powerline
brew cask install font-ubuntu
brew cask install font-ubuntu-mono-powerline
brew tap thii/fonts # san francisco font
brew cask install font-sanfrancisco

###############################################################################
# Utilities                                                                   #
###############################################################################
brew cask install backblaze
brew cask install cloudytabs
brew cask install disk-inventory-x
brew cask install filebot
brew cask install unetbootin
brew cask install karabiner
brew cask install cheatsheet
brew cask install hazel
brew cask install amazon-cloud-drive
brew cask install google-drive
brew cask install watchman
brew cask install easyfind
brew install ncdu
brew install tree
brew install mackup
brew install hr
brew install python

###############################################################################
# Audio, Video, Chat                                                          #
###############################################################################
brew cask install adium-nightly16
brew cask install jaikoz
brew cask install mpv
brew cask install skype
brew cask install spotify
brew cask install amazon-music
brew cask install google-photos-backup
brew cask install google-hangouts

###############################################################################
# Editors                                                                    #
###############################################################################
brew cask install textmate
brew cask install sourcetree
brew cask install github-desktop
brew cask install the-unarchiver

###############################################################################
# Quick look enhancements                                                     #
###############################################################################
brew cask install qlmarkdown
brew cask install qlstephen
brew cask install quicklook-json
brew cask install qlvideo
brew cask install qlcolorcode
brew cask install qlimagesize

###############################################################################
# Browsers                                                                    #
###############################################################################
brew cask install chrome-remote-desktop-host
brew cask install google-chrome-canary
brew cask install chromium
brew cask install webkit-nightly
brew cask install firefox
brew cask install opera
brew cask install torbrowser


###############################################################################
# Fun                                                                         #
###############################################################################
brew cask install transmission
brew cask install transmission-remote-gui
brew cask install panic-unison
